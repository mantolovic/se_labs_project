from django.contrib import admin

from .models import playlist, song, vote, comment

admin.site.register(song)
admin.site.register(playlist)
admin.site.register(vote)
admin.site.register(comment)