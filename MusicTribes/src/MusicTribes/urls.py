"""MusicTribes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from Account.views import register_view, login_view, logout_view, edit_user_view

from Tribe.views import TribesList, detail, join_tribe, create_tribe, delete_membership, kick_member, send_message, guest_list

from playlist.views import playlist_detail,create_playlist,add_song,delete_song,playlist_edit, delete_playlist, Like, send_comment

urlpatterns = [
    path('', TribesList, name='home'),
    path('admin/', admin.site.urls),
    path('register/', register_view, name='register'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('edit_user/', edit_user_view, name='edit_user'),

    

    path('guest_list/', guest_list, name='guest_list'),
    path('detail/<int:tribe_id>', detail, name='detail'),
    path('join_tribe/<int:tribe_id>', join_tribe, name='join_tribe'),
    path('leave_tribe/<int:tribe_id>', delete_membership, name='delete_membership'),
    path('kick_member/<int:tribe_id>,<int:id>', kick_member, name='kick_member'),
    path('create_tribe/', create_tribe, name='create_tribe'),
    path('send_message/<int:tribe_id>', send_message, name='send_message'),

    path('playlist/<int:id>', playlist_detail, name='playlist_detail'),
    path('edit_playlist/<int:id>', playlist_edit, name='playlist_edit'),
    path('create_playlist/<int:tribe_id>', create_playlist, name='create_playlist'),
    path('delete_playlist/<int:id>', delete_playlist, name='delete_playlist'),
    path('add_song/<int:id>', add_song, name='add_song'),
    path('delete_song/<int:id><int:chief_id>', delete_song, name='delete_song'),
    path('vote/<int:id><int:play_id>',Like , name="like"),
    path('send_comment/<int:id><int:play_id>', send_comment, name='send_comment'),
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
