from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import CreateTribeForm, CreateMessageForm
from django.urls import reverse
from .models import Membership, Tribe, Message
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib import messages
from playlist.models import playlist
from django.contrib.auth.decorators import login_required

@login_required(login_url="/login")
def send_message(request, tribe_id):
    TTribe=get_object_or_404(Tribe, pk=tribe_id)
    users=Membership.objects.filter(Tribe=TTribe).values('User')
    user_list=[]
    for i in users:
        user_list.append(i['User'])
    user_list.append(TTribe.chieftain.id)
    if request.user.id in user_list:
        if request.method== 'POST':
            message_form=CreateMessageForm(request.POST)
        
            if message_form.is_valid():
                NewMessage=message_form.save(commit=False)
                NewMessage.mess_to = get_object_or_404(Tribe, pk=tribe_id)
                NewMessage.mess_from=request.user
                NewMessage=message_form.save()
            else:
                messages.success(request, ('not valid form!'))
        return HttpResponseRedirect(reverse('detail', args=[tribe_id,]))
    else:
        messages.info(request, ('You are not a member in this tribe!'))
        return HttpResponseRedirect(reverse('detail', args=[tribe_id,]))


def guest_list(request):
    other_tribes=Tribe.objects.all()
    context={
        'other_tribes':other_tribes
    }
    return render (request, 'tribe_list_guest.html', context)


@login_required(login_url="/guest_list")
def TribesList(request):
    username=request.user.username
    tribes_list=Tribe.objects.all()
    my_tribes=[]
    for x in tribes_list:
        if x.chieftain.username==username:
            my_tribes.append(x)
    
    tribes_id=Membership.objects.filter(User=request.user).values_list("Tribe",flat=True)
    member_in_Tribes=[]
    for id in tribes_id:
        member_in_Tribes.append(get_object_or_404(Tribe, pk=id))
    enrolled_tribes=member_in_Tribes+my_tribes
    
    other_tribes=list(set(tribes_list) - set(enrolled_tribes))
    

    context ={
        'Tribes_list':my_tribes,
        'member_in_Tribes':member_in_Tribes,
        'other_tribes':other_tribes,
        'username':username
    }
    return render (request, 'tribe_list.html', context)


def detail (request,tribe_id):
    tribe=get_object_or_404(Tribe, pk=tribe_id)
    playlists=playlist.objects.filter(tribe=tribe).values('playlistName','playlist_desc','id')
    messages=Message.objects.filter(mess_to=tribe)
    message_form=CreateMessageForm()

    users=Membership.objects.filter(Tribe=tribe)

    context ={
        'Users':users,
        'Tribe':tribe,
        'playlists':playlists,
        'Messages':messages,
        'message_form':message_form,
    }
    return render (request, 'tribe_details.html', context)


@login_required(login_url="/login")
def create_tribe(request, *args, **kwargs):

    if request.method== 'POST':
        form=CreateTribeForm(request.POST, request.FILES)
        
        if form.is_valid():
            NewTribe=form.save(commit=False)
            NewTribe.chieftain = request.user
            NewTribe=form.save()
            return HttpResponseRedirect(reverse('detail', args=[NewTribe.tribe_id,]))
        else:
            messages.error(request, ('Nesto ne stima.'))
    else:
        form=CreateTribeForm()
        context= {'form':form}
        return render(request, "create_tribe.html",context)

@login_required(login_url="/login")
def join_tribe(request, tribe_id):
    tribe=Tribe.objects.get(tribe_id=tribe_id)
    myuser=User.objects.get(id=request.user.id)
    enroll=Membership(User=myuser, Tribe=tribe)
    enroll.save()
    return HttpResponseRedirect(reverse('detail', args=[tribe_id,]))

@login_required(login_url="/login")
def delete_membership(request, tribe_id): 
    context ={}
    name=request.user
    tribe=get_object_or_404(Tribe, tribe_id=tribe_id)
    obj = get_object_or_404(Membership, Tribe = tribe, User = name) 
  
    if request.method =="POST": 
        
        obj.delete()
        return HttpResponseRedirect("/") 
  
    return render(request, "delete_view.html", context) 

@login_required(login_url="/login")
def kick_member(request, tribe_id,id): 
    context ={}
    name=get_object_or_404(User, pk=id)
    tribe=get_object_or_404(Tribe, tribe_id=tribe_id)
    obj = get_object_or_404(Membership, Tribe = tribe, User = name) 
    if request.user == tribe.chieftain:

        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect(reverse('detail', args=[tribe_id,]))
  
        return render(request, "delete_view.html", context)
    else:
        messages.error(request, ("You are not chieftain of this tribe, you can't kick members out."))
        return HttpResponseRedirect(reverse('detail', args=[tribe_id,]))