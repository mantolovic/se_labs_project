from django.shortcuts import render, redirect,get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout,update_session_auth_hash
from django.contrib import messages
from django.conf import settings
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from .models import Profile

from .forms import EditUserForm
from .forms import CreateUserForm
# Create your views here.


def register_view(request, *args, **kwargs):
    form=CreateUserForm()

    if request.method== 'POST':
        form=CreateUserForm(request.POST)
        if form.is_valid():
            user=form.save()
            Profile.objects.create(
                user=user
            )
            messages.success(request, 'Your account has ben successfully registered!')
            return redirect('home')

    context= {'form':form}
    return render(request, "register.html",context)

def login_view(request, *args, **kwargs):
    
    if request.method=='POST':
        username=request.POST.get('username')
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('home')
        else:
             messages.info(request, "Incorrect username or password")
    context={}
    return render(request, "login.html", context)


def logout_view(request, *args, **kwargs):
    logout(request)
    return redirect('login')

def edit_user_view(request):
    user = get_object_or_404(User, username=request.user.username)
    if request.method == 'POST':
        
        form = PasswordChangeForm(request.user, request.POST)
        profile_form = EditUserForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('edit_user')
        if  profile_form.is_valid():
            profile_form.save()
            messages.success(request, ('Account updated successfully !'))
            return redirect('home')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        profile_form = EditUserForm(instance=request.user.profile)
        form = PasswordChangeForm(request.user)
    
    return render(request, 'edit_user.html', {
        'profile_form': profile_form,
        'edit_user_form':form,
        'picture':user.profile.image
    })